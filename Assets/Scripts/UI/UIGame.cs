﻿using System;
using UnityEngine;
using UnityEngine.UI;


public class UIGame : MonoBehaviour
{
    public Action<float> OnShouldMoveSimple; // duration
    public Action<float, int, float> OnShouldMoveZigZag; // duration , count , distance
    public Action<float, float, int> OnShouldMoveSpiral; // duration, radius, count


    [SerializeField]
    private Button simpleMoveButton = default;
    [SerializeField]
    private Button zigzagMoveButton = default;
    [SerializeField]
    private Button spiralMoveButton = default;

    [SerializeField]
    private InputField durationInputField = default;
    [SerializeField]
    private InputField countInputField = default;
    [SerializeField]
    private InputField distanceInputField = default;
    [SerializeField]
    private InputField radiusInputField = default;



    public void Initialize()
    {
        simpleMoveButton.onClick.AddListener(TriggerSimpleMoveEvent);
        zigzagMoveButton.onClick.AddListener(TriggerZigZagMoveEvent);
        spiralMoveButton.onClick.AddListener(TriggerSpiralMoveEvent);
    }


    public void Deinitialize()
    {
        simpleMoveButton.onClick.RemoveListener(TriggerSimpleMoveEvent);
        zigzagMoveButton.onClick.RemoveListener(TriggerZigZagMoveEvent);
        spiralMoveButton.onClick.RemoveListener(TriggerSpiralMoveEvent);
    }

    private void TriggerSimpleMoveEvent()
    {
        float duration = FloatParseField(durationInputField);

        OnShouldMoveSimple?.Invoke(duration);
    }


    private void TriggerZigZagMoveEvent()
    {
        float duration = FloatParseField(durationInputField);
        int count = IntParseField(countInputField);
        float distance = FloatParseField(distanceInputField);

        OnShouldMoveZigZag?.Invoke(duration, count, distance);
    }


    private void TriggerSpiralMoveEvent()
    {
        float duration = FloatParseField(durationInputField);
        int count = IntParseField(countInputField);
        float radius = FloatParseField(radiusInputField);

        OnShouldMoveSpiral?.Invoke(duration, radius, count);
    }


    private float FloatParseField(InputField inputField)
    {
        float result = 0;

        if (float.TryParse(inputField.text, out float res))
        {
            result = res;
        }
        else
        {
            Debug.LogWarning("IncorrectInput in field " + inputField);
            result = 0;
        }

        return result;
    }


    private int IntParseField(InputField inputField)
    {
        int result = 0;

        if (int.TryParse(inputField.text, out int res))
        {
            result = res;
        }
        else
        {
            Debug.LogWarning("IncorrectInput in field " + inputField);
            result = 0;
        }

        return result;
    }
}
