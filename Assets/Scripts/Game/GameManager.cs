﻿using UnityEngine;


public class GameManager : MonoBehaviour
{
    [SerializeField]
    private UIGame UIGame = default;
    [SerializeField]
    private Game game = default;



    private void Awake()
    {
        UIGame.Initialize();

        UIGame.OnShouldMoveSimple += DoSimpleMove;
        UIGame.OnShouldMoveZigZag += DoZigZagMove;
        UIGame.OnShouldMoveSpiral += DoSpiralMove;
    }


    private void OnDestroy()
    {
        UIGame.OnShouldMoveSimple -= DoSimpleMove;
        UIGame.OnShouldMoveZigZag -= DoZigZagMove;
        UIGame.OnShouldMoveSpiral -= DoSpiralMove;

        UIGame.Deinitialize();
    }


    private void DoSpiralMove(float duration, float radius, int turnsNumber)
    {
        game.SpiralMove(duration, radius, turnsNumber);
    }


    private void DoZigZagMove(float duration, int count, float distance)
    {
        game.ZigZagMove(duration, count, distance);
    }


    private void DoSimpleMove(float duration)
    {
        game.SimpleMove(duration);
    }
}
