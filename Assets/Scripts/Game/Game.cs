﻿using UnityEngine;
using DG.Tweening;
using System.Collections;
using System.Collections.Generic;

public class Game : MonoBehaviour
{
    [SerializeField]
    private Transform startPoint = default;
    [SerializeField]
    private Transform finishPoint = default;
    [SerializeField]
    private TrailRenderer ball = default;

    private Coroutine spiralCoroutine;



    public void SimpleMove(float duration)
    {
        SetupStartPosition();

        ball.transform
            .DOMove(finishPoint.position, duration)
            .SetEase(Ease.Linear);
    }


    public void ZigZagMove(float duration, int count, float distance)
    {
        Debug.Log(count);
        SetupStartPosition();

        float totalDistance = Vector3.Distance(startPoint.position, finishPoint.position);

        float distanceForTooth = totalDistance / count;

        List<Vector3> path = new List<Vector3>();

        Vector3 dir = (finishPoint.position - startPoint.position).normalized;

        for (int i = 0; i <= count; i++)
        {
            Vector3 pointOnLine = startPoint.position + dir * i * distanceForTooth;
            Vector3 pointTooth = pointOnLine + dir * distanceForTooth * 0.5f;

            Vector3 toothDir = Vector3.Cross(dir, i % 2 == 0 ? Vector3.forward : -Vector3.forward).normalized;
            pointTooth += toothDir * 2f;

            Debug.Log(pointOnLine);
            path.Add(pointOnLine);

            if (i != count)
            {
                path.Add(pointTooth);
            }
        }

        ball.transform
            .DOPath(path.ToArray(), duration)
            .SetEase(Ease.Linear);
    }


    private Vector3 GetProjected(Vector3 s, Vector3 f, Vector3 c)
    {
        Vector3 startToFinish = f - s;
        Vector3 prj = Vector3.Project(c - s, startToFinish);
        return prj + s;
    }

    public void SpiralMove(float duration, float radius, int turnsNumber)
    {
        SetupStartPosition();

        float radiusReduceSpeed = radius / duration;
        float angleSpeed = 180.0f * turnsNumber * Mathf.Abs((duration - 6.153846f) * 0.26f); // hz. ya reshal uravnenie i vidimo huiyvo reshal
        spiralCoroutine = StartCoroutine(MoveSpiral(radius, radiusReduceSpeed, angleSpeed));
    }
    

    private IEnumerator MoveSpiral(float radius, float radiusReduceSpeed, float angleSpeed)
    {
        float time = 0.0f;
        float currentAngle = 1.0f;
        float currentRadius = radius;
        Vector3 positionToSet = Vector3.zero;

        while (currentRadius >= 0.0f)
        {
            currentAngle += Time.deltaTime * angleSpeed;
            currentRadius -= Time.deltaTime * radiusReduceSpeed;

            positionToSet.x = finishPoint.position.x + Mathf.Cos(Mathf.Deg2Rad * currentAngle) * currentRadius;
            positionToSet.y = finishPoint.position.y + Mathf.Sin(Mathf.Deg2Rad * currentAngle) * currentRadius;
            positionToSet.z = ball.transform.position.z;

            ball.transform.position = positionToSet;

            time += Time.deltaTime;

            yield return null;
        }

        Debug.Log("<color=red>Duration " + time + "</color>");

        ball.transform.position = finishPoint.position;
        spiralCoroutine = null;
    }


    private void SetupStartPosition()
    {
        if (spiralCoroutine != null)
        {
            StopCoroutine(spiralCoroutine);
        }

        ball.transform.DOComplete();
        ball.transform.position = startPoint.position;

        ball.Clear();
    }
}
