﻿using UnityEngine;


public class DraggableObject : MonoBehaviour
{
    private Vector3 screenPoint;
    private Vector3 offset;
    private Camera mainCamera;



    private void Awake()
    {
        mainCamera = Camera.main;
    }


    private void OnMouseDown()
    {
        screenPoint = mainCamera.WorldToScreenPoint(gameObject.transform.position);
        offset = gameObject.transform.position - mainCamera.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z));

    }


    private void OnMouseDrag()
    {
        Vector3 curScreenPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z);

        Vector3 curPosition = mainCamera.ScreenToWorldPoint(curScreenPoint);
        transform.position = curPosition;
    }
}
